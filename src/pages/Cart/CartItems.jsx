import styles from '../../components/Card/card.module.scss'
import {ReactComponent as Star} from '../../img/13487794321580594410.svg'
import Button from '../../components/Button/Button';
import PropTypes from 'prop-types';
import {removeFromCart, openModal, addToFavorites, removeFromFavorites} from '../../redux/actions'
import { useEffect, useState } from 'react';
import { useDispatch, useSelector} from 'react-redux';

function Product({info}) {

    const dispatch = useDispatch();
    const favorites = useSelector((state) => state.favorites);

    const [color, setColor] = useState()
    const {id:itemId} = info;

    useEffect(() => {
        for (let i = 0; i <favorites.length; i++){
            if(favorites[i].id === itemId){
                setColor(true);
            }
        }
    }, [favorites, itemId])

    const changeColor = (color) =>{
        setColor(false);
    }

    function handler(){
        removeProduct();
        changeColor();
    }

    const addProductToFavorites = () => {
        dispatch(addToFavorites(info));
    }

    const removeProduct = () => {
        dispatch(removeFromFavorites(info));
    }


    const showModal = () =>{
        dispatch(openModal({type: "Видалити", payload: info}));
    }

    return (
        <div className={styles.cards}>
            <img className={styles.card__img} src={info.url} alt={info.name} />
            <div className={styles.card__header}>
                <h1 className={styles.card__name}>{info.name}</h1>
                <button className={styles.card__to_favorite} >
                    <Star className={color? styles.active: null} onClick={color? handler: addProductToFavorites}/>
                </button>
            </div>

            <p className={styles.card__text}> {info.shortDescription} </p>
            <p className={styles.card__price}> Ціна: {info.price} грн</p>
            <Button backgroundColor = 'gray' text = {'Видалити'} onClick={() => {showModal()}}
           />
        </div>

     );
}

Product.propTypes = {

    info: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        url: PropTypes.string.isRequired,
        shortDescription: PropTypes.string.isRequired
    }).isRequired,
}

export default Product;