import styles from './Table.module.scss'

function Table({children}){

 return (
      <table className={styles.table}>
        <thead className={styles.title}>
          <tr>
            <th className={styles.title}>Назва</th>
            <th className={styles.title}>Фото</th>
            <th className={styles.title}>Ціна</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
        {children}
        </tbody>
      </table>
    );
  };

  export default Table;