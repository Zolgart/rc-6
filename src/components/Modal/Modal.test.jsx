import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { useDispatch, useSelector } from 'react-redux';
import Modal from './Modal';
import { closeModal } from '../../redux/actions';
import renderer from 'react-test-renderer';


jest.mock('react-redux', () => ({
    ...jest.requireActual('react-redux'),
    useDispatch: jest.fn(),
    useSelector: jest.fn(),
  }));

describe('Modal component', () => {

    const mockDispatch = jest.fn();
    useDispatch.mockReturnValue(mockDispatch);


  it('renders with correct text when modal is active', () => {
    useSelector.mockReturnValue(true);
    render(
      <Provider store={configureStore([])({ modal: true })}>
        <Modal text="Hello, dear customer!" />
      </Provider>
    );

    const modalText = screen.getByText('Hello, dear customer!');
    expect(modalText).toBeInTheDocument();
  });

  it('closes modal when "Скасувати" button is clicked', () => {
    useDispatch.mockReturnValue(mockDispatch);
    useSelector.mockReturnValue(true);
    render(
      <Provider store={configureStore([])({ modal: true })}>
        <Modal text="Test" />
      </Provider>
    );

    const deleteButton = screen.getByText('Скасувати');
    fireEvent.click(deleteButton);

    expect(mockDispatch).toHaveBeenCalledWith(closeModal(false));
  });
});

describe('Modal component snapshot', () => {
    const mockDispatch = jest.fn();
    useDispatch.mockReturnValue(mockDispatch);

    it('renders correctly', () => {
      useSelector.mockReturnValue(true); 

      const tree = renderer.create(
        <Modal text="Test" />
      ).toJSON();

      expect(tree).toMatchSnapshot();
    });

  });