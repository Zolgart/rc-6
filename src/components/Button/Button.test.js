import React from 'react';
import { render, screen, fireEvent} from "@testing-library/react";
import renderer from 'react-test-renderer';
import Button from "./Button";


test('renders button with given text and background color', () => {
    const buttonText = 'Click me';
    const backgroundColor = 'blue';
    const onClick = jest.fn();

    render(
      <Button text={buttonText} backgroundColor={backgroundColor} onClick={onClick} />
    );

    const buttonElement = screen.getByRole('button');
    expect(buttonElement).toBeInTheDocument();
    expect(buttonElement).toHaveStyle(`background-color: ${backgroundColor}`);
    expect(screen.getByText(buttonText)).toBeInTheDocument();
    fireEvent.click(buttonElement);
    expect(onClick).toHaveBeenCalledTimes(1);
  });


  test('Button component renders correctly', () => {
    const component = renderer.create(
      <Button backgroundColor="blue" text="Click me" onClick={() => {}} />
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });